﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SceneButton : MonoBehaviour
{

    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;

    // Use this for initialization
    void Start () {
		
	}

    public void ParkButton ()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                SceneManager.LoadScene("ParkScene1");
                timer = 0f;
            }
        }
    }

    public void CityButton ()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                SceneManager.LoadScene("CityScene1");
                timer = 0f;
            }
        }
    }

    public void OnPointerEnter()
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit()
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }

}
