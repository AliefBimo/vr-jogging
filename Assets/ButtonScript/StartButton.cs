﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StartButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;

    public GameObject MainMenu;
    public GameObject SelectArea;
    public Text score;



    // Use this for initialization
    void Start () {
        SelectArea.SetActive(false);
        score.text = PlayerPrefs.GetString("Area") + PlayerPrefs.GetString("Time") + PlayerPrefs.GetString("KCAL");


    }

	void Update () {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                SelectArea.SetActive(true);
                MainMenu.SetActive(false);
                
                timer = 0f;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}
