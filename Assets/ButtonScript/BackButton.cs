﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class BackButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;
    public GameObject Result;
    public GameObject Canvas;
    public Text caloriesCount;
    public Text timerText;
    public Text totalCalories;
    public Text totalTime;
    

    // Use this for initialization
    void Start()
    {
        Result.SetActive(false);
        Scene currentScene = SceneManager.GetActiveScene();
        if(currentScene.name == "ParkScene1")
        {
            PlayerPrefs.SetString("Area", "Area: Park, ");
        }else 
        if (currentScene.name == "CityScene1")
        {
            PlayerPrefs.SetString("Area", "Area: City, ");
        }
        else
        if (currentScene.name == "PENS_Scene")
        {
            PlayerPrefs.SetString("Area", "Area: Kampus PENS, ");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                Canvas.SetActive(false);
                Result.SetActive(true);
                totalCalories.text = caloriesCount.text;
                totalTime.text = timerText.text;
                PlayerPrefs.SetString("Time", totalTime.text + ", ");
                PlayerPrefs.SetString("KCAL", totalCalories.text+ " ");
                
                timer = 0f;
            }
        }

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}
