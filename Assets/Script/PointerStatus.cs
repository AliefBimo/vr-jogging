﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PointerStatus : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool gazedAt = false;
    // Use this for initialization
    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
        //SceneButton sceneButton = new SceneButton(gazedAt);
        //sceneButton.ParkButton();

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
       // SceneButton sceneButton = new SceneButton(gazedAt);

    }
}

