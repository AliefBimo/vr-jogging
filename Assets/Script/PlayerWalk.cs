﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerWalk : MonoBehaviour
{
    public int playerSpeed;
    Rigidbody m_Rigidbody;
   // public GameObject CanvasWeight;
    //public GameObject Cam;

    private float shakeDetectionThreshold = 0.5f;
    private float accelerometerUpdateInterval = 1.0f / 60.0f;
    private float lowPassKernelWidthInSeconds = 0.2f;
    private float lowPassFilterFactor;
    private Vector3 lowPassValue;
    
    // Use this for initialization
    void Start()
    {
        //m_Rigidbody = GetComponent<Rigidbody>();
        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        shakeDetectionThreshold *= shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 acceleration = Input.acceleration;
        Debug.Log("acceleration : " + acceleration);
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Debug.Log("LowpassValue : "+lowPassValue);
        Vector3 deltaAcceleration = acceleration - lowPassValue;
        Debug.Log("deltaAcceleration : " + deltaAcceleration.sqrMagnitude);

        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold)
        {
            Debug.Log("True");
            if (playerSpeed <= 10)
            {
                //Time.timeScale = 1;
                playerSpeed = playerSpeed + 1;
            }
        }
        else
        {
            if (playerSpeed > 0)
            {
                playerSpeed = playerSpeed - 1;
            }
            //else
            //{
            //   if(playerSpeed == 0)
            //    {
            //        Time.timeScale = 0;
            //   }
            //}
        }
        //m_Rigidbody.velocity = Camera.main.transform.forward * playerSpeed;
        //Cam.transform.position = Camera.main.transform.forward * playerSpeed;
        
        transform.position = transform.position + Camera.main.transform.forward * playerSpeed * Time.deltaTime;

        

    }
}
