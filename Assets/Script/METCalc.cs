﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class METCalc : MonoBehaviour {

    public Text caloriesCount;
    public int weight;
    public double ECS;
    public double usedCalories;
    public TextMeshProUGUI InputText;
    public string caloriesBurned;
    public GameObject CanvasWeight;
    public GameObject Result;
    public float elapsed = 0f;

    private float shakeDetectionThreshold = 0.5f;
    private float accelerometerUpdateInterval = 1.0f / 60.0f;
    private float lowPassKernelWidthInSeconds = 0.5f;
    private float lowPassFilterFactor;
    private Vector3 lowPassValue;
    private float startTime;

    // Use this for initialization
    void Start()
    {
        //startTime = Time.time;
        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        shakeDetectionThreshold *= shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
        //InputText.text = "";
        usedCalories = 0;
        caloriesBurned = "";

    }

    // Update is called once per frame
    void Update()
    {
        if (CanvasWeight.activeSelf == false && Result.activeSelf == false)
        {
            if (!string.IsNullOrEmpty(InputText.text))
            {
                weight = int.Parse(InputText.text);
                calories_CALC(weight);
                //Debug.Log(weight);
            }
        }
    }

    public void calories_CALC(int weight)
    {
       
        Vector3 acceleration = Input.acceleration;
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Vector3 deltaAcceleration = acceleration - lowPassValue;
        
        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold)
        {
            elapsed += Time.deltaTime;
            if (elapsed >= 1f)
            {
                elapsed = elapsed % 1f;
                ECS = ((0.13 * 2.7 * this.weight) / 200)/60 ;
                usedCalories += ECS;
            }
           
        } 
        //string formatCALC =
        caloriesBurned = usedCalories.ToString("f2");

        caloriesCount.text = "KCAL : " + caloriesBurned;
    }
}
