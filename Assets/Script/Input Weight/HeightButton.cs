﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class HeightButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;
    public TextMeshProUGUI InputText;
    public TextMeshProUGUI Height;
    private string _currentInput;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                _currentInput = InputText.text;
                if (!string.IsNullOrEmpty(_currentInput))
                {
                    Height.text = _currentInput;
                }

                timer = 0f;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}

