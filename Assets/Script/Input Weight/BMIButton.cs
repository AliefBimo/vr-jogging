﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class BMIButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;

    public GameObject BMI;
    public GameObject MainMenu;

    // Use this for initialization
    void Start () {
        BMI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {

                BMI.SetActive(true);
                MainMenu.SetActive(false);

                timer = 0f;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}
