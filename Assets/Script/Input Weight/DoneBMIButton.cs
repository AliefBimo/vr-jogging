﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class DoneBMIButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{ 

    // Use this for initialization
    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;
    public float weight;
    public float height;
    public float sqrHeight;
    public float BMI;

    //public GameObject MainMenu;
   // public GameObject Canvas;
   // public GameObject Weight;
    public TextMeshProUGUI InputText;
    public TextMeshProUGUI InputHeight;
    public TextMeshProUGUI InputWeight;

    // Use this for initialization
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                height = float.Parse(InputHeight.text) / 100;
                weight = float.Parse(InputWeight.text);
                sqrHeight = height * height;
                BMI = weight / sqrHeight;
                InputText.text = "BMI = " + BMI.ToString("f2");

                timer = 0f;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}
