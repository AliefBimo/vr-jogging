﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class Button9 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public float gazeTime = 2f;
    private float timer = 0f;
    private bool gazedAt = false;
    public TextMeshProUGUI InputText;
    private string _currentInput;
    private string value = "9";
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                _currentInput = InputText.text;
                if (!string.IsNullOrEmpty(_currentInput))
                {
                    if (_currentInput.Length <= 3)
                    {
                        _currentInput += value;
                    }
                }
                else
                {
                    _currentInput = value;
                }
                InputText.text = _currentInput;

                timer = 0f;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("pointer enter");
        gazedAt = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("pointer exit");
        gazedAt = false;
    }
}
