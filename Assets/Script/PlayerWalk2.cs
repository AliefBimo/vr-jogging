﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk2 : MonoBehaviour {

    public int playerSpeed;

    private double derivativeThreshold = 0.05;
    private float elapse;
    private double deltaDerivative;
    private float accT1;
    private float accT2;
    private float accY;
    private float derAcc;
    private float max;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        accY = Input.acceleration.y;
        elapse += Time.fixedDeltaTime;
        deltaDerivative = elapse - derivativeThreshold;

        if( elapse > deltaDerivative )
        {
            accT1 = accY;
            derAcc = Mathf.Abs((accT1 - accT2) / elapse);
            max = Mathf.Max(derAcc, max);
            accT2 = accY;
            elapse = 0;
        }

        if(accY > -1.3)
        {
            playerSpeed = 10;
            transform.position = transform.position + Camera.main.transform.forward * playerSpeed * Time.deltaTime;
        }
        else if (accY < -0.7)
        {
            playerSpeed = 0;
            transform.position = transform.position + Camera.main.transform.forward * playerSpeed * Time.deltaTime;
        }

        
	}
}
